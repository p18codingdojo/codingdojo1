class MasterMind() {

    fun evaluate(secret: Array<Color>, guess: Array<Color>): IntArray {
        if (sameSize(secret, guess)) throw IllegalArgumentException()

        val colorsInSecret = ArrayList<Color>()
        colorsInSecret.addAll(secret)

        var matches = Pair(0, 0)

        for (i in secret.indices) {
            matches = countMatches(guess, i, secret, colorsInSecret, matches)
        }
        return intArrayOf(matches.first, matches.second)

    }

    private fun countMatches(guess: Array<Color>, i: Int, secret: Array<Color>, colorsInSecret: MutableList<Color>, matches: Pair<Int, Int>): Pair<Int, Int> {
        var positonMatches = matches.first
        var colorMatches = matches.second
        val currentPin = guess.get(i)
        val secretPinAtPosition = secret.get(i)
        if (currentPin == secretPinAtPosition) {
            positonMatches++;
        } else {
            if (colorsInSecret.contains(currentPin)) {
                colorMatches++;
                colorsInSecret.remove(currentPin)
            }
        }
        return Pair(positonMatches, colorMatches)
    }

    private fun sameSize(secret: Array<Color>, guess: Array<Color>) = secret.size != guess.size
}

enum class Color {
    RED,
    BLUE,
    GREEN,
    YELLOW,
    ORANGE,
    BROWN
}
