import org.junit.Test
import kotlin.test.assertFailsWith

class MasterMindTest {

    @Test
    fun testThatEvaluateReturns00() {
        val result = MasterMind().evaluate(arrayOf(), arrayOf())
        assert(result contentEquals intArrayOf(0, 0))
    }

    @Test
    fun testThatColorsExist() {
        Color.RED
        Color.BLUE
        Color.GREEN
        Color.YELLOW
        Color.ORANGE
        Color.BROWN
    }

    @Test
    fun testThatEvaluateThrowsIfArgumentsHaveDifferentLength() {
        assertFailsWith(IllegalArgumentException::class) {
            MasterMind().evaluate(arrayOf(Color.RED), arrayOf())
        }
    }

    @Test
    fun testThatEvaluateDoesntThrowIfArgumentsHaveSameLength() {
        MasterMind().evaluate(arrayOf(), arrayOf())
    }

    @Test
    fun testThat_Red_RedReturns10() {
        val evaluate = MasterMind().evaluate(arrayOf(Color.RED), arrayOf(Color.RED))
        assert(evaluate contentEquals intArrayOf(1, 0))
    }

    @Test
    fun testThat_Blue_RedReturns00() {
        val evaluate = MasterMind().evaluate(arrayOf(Color.BLUE), arrayOf(Color.RED))
        assert(evaluate contentEquals intArrayOf(0, 0))
    }

    @Test
    fun testThat_RedBlue_RedBlue_Returns20() {
        val evaluate = MasterMind().evaluate(arrayOf(Color.RED, Color.BLUE), arrayOf(Color.RED, Color.BLUE))
        assert(evaluate contentEquals intArrayOf(2, 0))
    }

    @Test
    fun testThat_RedBlue_BlueRed_Returns02() {
        val evaluate = MasterMind().evaluate(arrayOf(Color.RED, Color.BLUE), arrayOf(Color.BLUE, Color.RED))
        assert(evaluate contentEquals intArrayOf(0, 2))
    }

    @Test
    fun testThat_RedRedBlueBlue_BrownBrownRedRed_Returns02() {
        val evaluate = MasterMind().evaluate(arrayOf(Color.RED, Color.RED, Color.BLUE, Color.BLUE),
                arrayOf(Color.BROWN, Color.BROWN, Color.RED, Color.RED))
        assert(evaluate contentEquals intArrayOf(0, 2))
    }

    @Test
    fun testThat_RedRedBlue_BrownBrownRed_Returns02() {
        val evaluate = MasterMind().evaluate(arrayOf(Color.RED, Color.RED, Color.BLUE),
                arrayOf(Color.BROWN, Color.BROWN, Color.RED))
        assert(evaluate contentEquals intArrayOf(0, 1))
    }

    @Test
    fun testThat_RedBlueBlue_BrownRedRed_Returns02() {
        val evaluate = MasterMind().evaluate(arrayOf(Color.RED, Color.BLUE, Color.BLUE),
                arrayOf(Color.BROWN, Color.RED, Color.RED))
        assert(evaluate contentEquals intArrayOf(0, 1))
    }


//    evaluate([blue], [red]) should return [0, 0]
//    evaluate([blue], [blue]) should return [1, 0]
//    evaluate([red, yellow], [blue, red]) should return [0, 1]

    @Test
    fun accTest1() {
        val evaluate = MasterMind().evaluate(arrayOf(Color.BLUE),
                arrayOf(Color.RED))
        assert(evaluate contentEquals intArrayOf(0, 0))
    }

    @Test
    fun accTest2() {
        val evaluate = MasterMind().evaluate(arrayOf(Color.BLUE),
                arrayOf(Color.BLUE))
        assert(evaluate contentEquals intArrayOf(1, 0))
    }

    @Test
    fun accTest3() {
        val evaluate = MasterMind().evaluate(arrayOf(Color.RED, Color.YELLOW),
                arrayOf(Color.BLUE, Color.RED))
        assert(evaluate contentEquals intArrayOf(0, 1))
    }

    @Test
    fun accTest4() {
        val evaluate = MasterMind().evaluate(arrayOf(Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW),
                arrayOf(Color.YELLOW, Color.ORANGE, Color.GREEN, Color.BROWN))
        assert(evaluate contentEquals intArrayOf(1, 1))
    }

    @Test
    fun accTest5() {
        val evaluate = MasterMind().evaluate(
                arrayOf(Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW, Color.YELLOW),
                arrayOf(Color.YELLOW, Color.YELLOW, Color.ORANGE, Color.GREEN, Color.BROWN))
        assert(evaluate contentEquals intArrayOf(0, 3))
    }

}




